package com.example.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * 多线程异步处理时的事务管理
 */
@Slf4j
@SpringBootTest
class MultiThreadTransactionComponentTest {

    @Autowired
    PlatformTransactionManager platformTransactionManager;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Test
    public void testTransaction() {
        // 定义此主线程的 异步处理时事务管理
        MultiThreadTransactionComponent mt = new MultiThreadTransactionComponent(platformTransactionManager, threadPoolTaskExecutor);

        for (int k = 0; k < 10; k++) {
            int i = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            // 添加任务
            mt.addFunction(() -> {
                System.out.println("当前线程：" + Thread.currentThread().getName());
                System.out.println(i + "--" + y);
                //除数为0时 执行失败
                System.out.println(i % y);
                return 0;
            });
        }

        // 执行任务
        mt.execute();

    }

}
