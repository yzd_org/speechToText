package com.example.demo.socket;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.model.Message;
import com.example.demo.utils.StringUtil;


/**
 * 公共逻辑
 *
 * @author yzd
 */
public abstract class BaseController extends AbstractWsController {

    private static final String CONNECT_TYPE_TEXT = "text";

    private static final String CONNECT_TYPE_AUDIO = "audio";

    /**
     * 接受客户端发送的字符串
     *
     * @param message 字符串消息
     */
    @Override
    protected void onMessage(String message) {
        Message msg = JSONObject.parseObject(message, Message.class);
        msg.setHost(getUserName());
        if (CONNECT_TYPE_TEXT.equals(getConnectType())) {
            msg.setMsg(StringUtil.txt2htm(msg.getMsg()));
            if (msg.getDests() == null) {
                broadcast2All(msg.toString());
            } else {
                broadcast2Special(msg.toString(), msg.getDests());
            }
        }
        // 音频转写 发送给自己
        else if (CONNECT_TYPE_AUDIO.equals(getConnectType())) {
            msg.setMsg(StringUtil.txt2htm(msg.getMsg()));
            broadcastTome(msg.toString());
        } else {
            broadcast2Others(msg.toString());
        }
    }

}
