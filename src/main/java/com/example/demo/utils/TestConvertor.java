package com.example.demo.utils;

import java.io.IOException;

/**
 * @author zbwd-25
 */
public class TestConvertor {

    public void testPcm2Wav(){
        short numChannels = 1;
        int sampleRate = 16000;
        short bitsPerSample = 8;
        String src = "/Users/wang/example.pcm";
        String target = "/Users/wang/example.wav";
        try {
            int result = Converter.pcmToWavByFile(numChannels, sampleRate, bitsPerSample, src, target);
//            Assert.assertEquals(0, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
